import { createReducer, on } from "@ngrx/store"
import { Action } from "rxjs/internal/scheduler/Action"
import { setErrorMessage, setLoadingSpenner } from "./shared.actions"
import { initialState } from "./shared.state"

const _sharedReducer = createReducer(
  initialState,
  on(setLoadingSpenner, (state, action) => ({
    ...state, 
    showLoading: action.status
  })),
  on(setErrorMessage, (state, action) => ({
    ...state, 
    errorMessage: action.message
  })),
)

export function sharedReducer (state: any, action: any) {
  return _sharedReducer(state, action)
}