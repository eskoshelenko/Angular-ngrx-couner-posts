import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/store/app.state';
import { setLoadingSpenner } from 'src/app/store/Shared/shared.actions';
import { loginStart } from '../state/auth.actions';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm!: FormGroup

  constructor(private store: Store<AppState>) { }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      email: new FormControl(null, [
        Validators.required,
        Validators.email,
      ]),
      password: new FormControl(null, [
        Validators.required,
        Validators.minLength(8)
      ])
    })
  }

  showEmailErrors(): string | void {
    const emailForm = this.loginForm.get('email')

    if(emailForm?.touched && !emailForm.valid) {
      if(emailForm.errors!['required']) {
        return 'Email required!'
      }
      if(emailForm.errors!['email']) {
        return 'Data should be email!'
      }
    }
  }

  showPasswordErrors(): string | void {
    const passwordForm = this.loginForm.get('password')

    if(passwordForm?.touched && !passwordForm.valid) {
      if(passwordForm.errors!['required']) {
        return 'Password required!'
      }
      if(passwordForm.errors!['minlength']) {
        return 'Password min length 8 characters!'
      }
    }
  }

  onLoginSubmit() {
    const email = this.loginForm.value.email
    const password = this.loginForm.value.password

    this.store.dispatch(setLoadingSpenner({ status: true }))
    this.store.dispatch(loginStart({email, password}))
  }
}
