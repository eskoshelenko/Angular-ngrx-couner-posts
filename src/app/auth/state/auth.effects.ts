import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { Store } from "@ngrx/store";
import { catchError, exhaustMap, map, mergeMap, of, tap } from "rxjs";
import { AuthService } from "src/app/services/auth.servoce";
import { AppState } from "src/app/store/app.state";
import { setErrorMessage, setLoadingSpenner } from "src/app/store/Shared/shared.actions";
import { autoLogin, autoLogout, loginFail, loginStart, loginSuccess, signUpStart, signUpSuccess } from "./auth.actions";

@Injectable()
export class AuthEffects {
  constructor(
    private actions$: Actions, 
    private authService: AuthService,
    private store: Store<AppState>,
    private router: Router
  ){}


  login$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(loginStart),
      exhaustMap((action) => {
        return this.authService
          .login(action.email, action.password)
          .pipe(
            map(data => {
              const user = this.authService.formatUser(data)
  
              this.store.dispatch(setLoadingSpenner({ status: false }))
              this.store.dispatch(setErrorMessage({ message: '' }))
              this.authService.setUserInLocalStorage(user)
              
              return loginSuccess({ user, redirect: true })
            }),
            catchError(errRes => {
              const message = errRes.error.error.message
              const errMessage = this.authService.getErrorMessage(message)

              this.store.dispatch(setLoadingSpenner({ status: false }))

              return of(setErrorMessage({ message: errMessage }))
            }),
          )
      })
    )
  })

  actionRedirect$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(loginSuccess, signUpSuccess),
      tap(action => {
        this.store.dispatch(setErrorMessage({ message: '' }))

        if (action.redirect) {
          this.router.navigate(['/'])
        }
      })
    )
  }, { dispatch: false })

  signUp$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(signUpStart),
      exhaustMap(action => {
        return this.authService
          .signup(action.email, action.password)
          .pipe(
            map(data => {
              const user = this.authService.formatUser(data);

              this.store.dispatch(setLoadingSpenner({ status: false }));
              this.store.dispatch(setErrorMessage({ message: '' }));
              this.authService.setUserInLocalStorage(user)

              return signUpSuccess({ user, redirect: true });
            }),
            catchError(errRes => {
              const message = errRes.error.error.message;
              const errMessage = this.authService.getErrorMessage(message);

              this.store.dispatch(setLoadingSpenner({ status: false }));

              return of(setErrorMessage({ message: errMessage }));
            })
          )
      })
    )
  })

  autoLogin$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(autoLogin),
      mergeMap(action => {
        const user = this.authService.getUserFromLocalStorage()
        
        return of(loginSuccess({ user, redirect: false }))
      })
    )
  })

  logout$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(autoLogout),
      map(action => {
        this.authService.logout()
        this.router.navigate(['auth'])
      })
    )
  }, { dispatch: false })
}