import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/store/app.state';
import { setLoadingSpenner } from 'src/app/store/Shared/shared.actions';
import { signUpStart } from '../state/auth.actions';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  signUpForm!: FormGroup

  constructor(private store: Store<AppState>) { }

  ngOnInit(): void {
    this.signUpForm = new FormGroup({
      email: new FormControl(null, [
        Validators.required, 
        Validators.email
      ]),
      password: new FormControl(null, [
        Validators.required, 
        Validators.minLength(8)
      ])
    })
  }

  showEmailErrors(): string | void {
    const emailForm = this.signUpForm.get('email')

    if(emailForm?.touched && !emailForm.valid) {
      if(emailForm.errors!['required']) {
        return 'Email required!'
      }
      if(emailForm.errors!['email']) {
        return 'Data should be email!'
      }
    }
  }

  showPasswordErrors(): string | void {
    const passwordForm = this.signUpForm.get('password')

    if(passwordForm?.touched && !passwordForm.valid) {
      if(passwordForm.errors!['required']) {
        return 'Password required!'
      }
      if(passwordForm.errors!['minlength']) {
        return 'Password min length 8 characters!'
      }
    }
  }

  onSignUpSubmit() {
    if (!this.signUpForm.valid) {
      return
    }
    
    const { email, password } = this.signUpForm.value
    
    this.store.dispatch(setLoadingSpenner({ status: true }))
    this.store.dispatch(signUpStart({ email, password }))
  }
}
