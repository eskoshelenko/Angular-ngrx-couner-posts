import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AppState } from 'src/app/store/app.state';
import { changeText, customIncrement } from '../state/counter.actions';
import { getText } from '../state/counter.selectors';
import { CounterState } from '../state/counter.state';

@Component({
  selector: 'app-counter-custom-input',
  templateUrl: './counter-custom-input.component.html',
  styleUrls: ['./counter-custom-input.component.css']
})
export class CounterCustomInputComponent implements OnInit {
  value: number = 0
  text$?: Observable<string>
  // text: string = ''

  constructor(private store: Store<AppState>) { }

  ngOnInit(): void {
    this.text$ = this.store.select(getText)
    // this.store.select(getText).subscribe(text => {
    //   this.text = text
    // })
  }

  onAdd() {
    this.store.dispatch(customIncrement({value: Number(this.value)}))
  }

  onChangeText() {
    this.store.dispatch(changeText({text: String(this.value)}))
  }
}
