import { createReducer, on } from "@ngrx/store"
import { changeText, customIncrement, decrement, increment, reset } from "./counter.actions"
import { initialState } from "./counter.state"

const _counterReducer = createReducer(
  initialState,
  on(
    increment,
    (state) => ({ ...state, counter: state.counter + 1 })
  ),
  on(
    decrement,
    (state) => ({ ...state, counter: state.counter - 1 })
  ),
  on(
    reset,
    (state) => ({ ...state, counter: 0 })
  ),
  on(
    customIncrement,
    (state, action) => ({ ...state, counter: state.counter + action.value })
  ),
  on(
    changeText,
    (state, action) => ({ ...state, text: action.text })
  ),
)

export function counterReducer(state: any, action: any) {
  return _counterReducer(state, action)
}