import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { map, Observable } from "rxjs";
import { Post } from "../models/posts.model";

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  private baseUrl = 'https://test-project-bcf60-default-rtdb.europe-west1.firebasedatabase.app'
  private path = '/posts.json'

  constructor(private http: HttpClient){}

  getPosts(): Observable<Post[]> {
    return this.http.get<Post[]>(`${this.baseUrl}${this.path}`)
      .pipe(
        map((data) => {
          const posts: Post[] = []

          for (const key in data) {
            posts.push({ ...data[key], id: key })
          }

          return posts
        })
      )
  }

  addPost(post: Post): Observable<{name: string}> {
    return this.http.post<{name: string}>(`${this.baseUrl}${this.path}`, post)
  }

  updatePost(post: Post) {
    const { id, title, description } = post
    const postData = {[id!]: {title, description}}

    return this.http.patch(`${this.baseUrl}${this.path}`, postData)
  }

  deletePost(id: string) {
    const path = '/posts/'

    return this.http.delete(`${this.baseUrl}${path}${id}.json`)
  }

  getPostById(id: string): Observable<Post> {
    const path = '/posts/'

    return this.http.get<Post>(`${this.baseUrl}${path}${id}.json`)
  }
}