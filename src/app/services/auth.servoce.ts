import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { autoLogout } from "../auth/state/auth.actions";
import { AuthResponseData } from "../models/auth.response.data";
import { User } from "../models/user.model";
import { AppState } from "../store/app.state";

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private base_url: string = 'https://identitytoolkit.googleapis.com/v1'
  private api_key: string = environment.FIREBASE_API_KEY
  private options = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  timeoutInterval: any

  constructor(
    private http: HttpClient,
    private store: Store<AppState>
  ) {}

  login(email: string, password: string): Observable<AuthResponseData> {
    const path = '/accounts:signInWithPassword?key='
    return this.http.post<AuthResponseData>(
      `${this.base_url}${path}${this.api_key}`, 
      { email, password, returnSecureToken: true }, 
      this.options
    )
  }

  signup(email: string, password: string): Observable<AuthResponseData> {
    const path = '/accounts:signUp?key='
    return this.http.post<AuthResponseData>(
      `${this.base_url}${path}${this.api_key}`, 
      { email, password, returnSecureToken: true }, 
      this.options
    )
  }

  logout() {
    localStorage.removeItem('userData')

    if (this.timeoutInterval) {
      clearTimeout(this.timeoutInterval)
      this.timeoutInterval = null
    }
  }

  formatUser(data: AuthResponseData) {
    const { email, idToken, localId, expiresIn } = data
    const expirationDate = new Date(new Date().getTime() + +expiresIn * 1000)
    const user = new User(email, idToken, localId, expirationDate)

    return user
  }

  runTimeoutInterval(user: User) {
    const currentDate = new Date().getTime()
    const expirationDate = new Date(user.getExpirationDate()).getTime()
    const timeInterval = expirationDate - currentDate 

    this.timeoutInterval = setTimeout(() => {
      this.store.dispatch(autoLogout())
      // logout functionality or get a refresh token
    }, timeInterval)
  }

  setUserInLocalStorage(user: User) {
    localStorage.setItem('userData', JSON.stringify(user))
    this.runTimeoutInterval(user)
  }

  getUserFromLocalStorage() {
    const userDataString = localStorage.getItem('userData');
    if (userDataString) {
      const { email, token, localId, expirationDate } = JSON.parse(userDataString);
      const user = new User( email, token, localId, expirationDate );

      this.runTimeoutInterval(user);
      
      return user;
    }
    return null;
  }

  getErrorMessage(message: string) {
    switch (message) {
      case 'EMAIL_NOT_FOUND': return 'There is no user record corresponding to this identifier. The user may have been deleted.'
      case 'EMAIL_EXISTS': return 'The email address is already in use by another account.'
      case 'OPERATION_NOT_ALLOWED': return 'Password sign-in is disabled for this project.'
      case 'TOO_MANY_ATTEMPTS_TRY_LATER': return 'We have blocked all requests from this device due to unusual activity. Try again later.'
      case 'INVALID_PASSWORD': return 'The password is invalid or the user does not have a password.'
      case 'USER_DISABLED': return 'The user account has been disabled by an administrator.'
      default: return 'Unknown error ocuured. Please try again.'
    }
  }
}