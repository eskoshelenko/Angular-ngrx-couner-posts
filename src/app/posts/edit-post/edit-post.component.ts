import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { Post } from 'src/app/models/posts.model';
import { AppState } from 'src/app/store/app.state';
import { updatePost, updatePostSuccess } from '../state/post.actions';
import { getPostById } from '../state/posts.selectors';

@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: ['./edit-post.component.css']
})
export class EditPostComponent implements OnInit, OnDestroy {
  post?: Post
  postForm!: FormGroup
  postSubscription?: Subscription

  constructor(
    private store: Store<AppState>,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.createForm()
    this.postSubscription = this.store.select(getPostById).subscribe((post) => {
      if (post) {
        this.post = post

        this.postForm.patchValue({
          title: post?.title,
          description: post?.description
        })
      }
    })
  }

  ngOnDestroy(): void {
    if (this.postSubscription) [
      this.postSubscription.unsubscribe()
    ]
  }

  createForm() {
    this.postForm = new FormGroup({
      title: new FormControl(this.post?.title, [
        Validators.required, 
        Validators.minLength(6)
      ]),
      description: new FormControl(this.post?.description, [
        Validators.required, 
        Validators.minLength(10)
      ]),
    })
  }

  showTitleErrors(): string|void {
    const titleForm = this.postForm.get('title')

    if (titleForm?.touched && !titleForm.valid) {
      if (titleForm.errors!['required']) {
        return 'Title is required!'
      }
      if (titleForm.errors!['minlength']) {
        return 'Title should be minimum 6 characters!'
      }
    }
  }

  showDescriptionErrors(): string|void {
    const descriptionForm = this.postForm.get('description')

    if (descriptionForm?.touched && !descriptionForm.valid) {
      if (descriptionForm.errors!['required']) {
        return 'Description is required!'
      }
      if (descriptionForm.errors!['minlength']) {
        return 'Description should be minimum 10 characters!'
      }
    }
  }

  onUpdatePost() {
    if (!this.postForm.valid) {
      return
    }

    const id = this.post!.id
    const { title, description } = this.postForm.value
    const post: Post = { id, title, description }

    this.store.dispatch(updatePost({post}))
    this.router.navigate(['posts'])
  }
}
