import { Action, createReducer, on } from "@ngrx/store"
import { addPostSuccess, deletePostSuccess, loadPostsSuccess, updatePostSuccess } from "./post.actions"
import { initialState, postsAdapter, PostState } from "./posts.state"

const _postsReducer = createReducer(
  initialState,
  on(addPostSuccess, (state, action) => {
    return postsAdapter.addOne(action.post, {...state, count: state.count++})
  }),
  on(updatePostSuccess, (state, action) => {
    return postsAdapter.updateOne(action.post, state)
  }),
  on(deletePostSuccess, (state, action) => {
    return postsAdapter.removeOne(action.id, state)
  }),
  on(loadPostsSuccess, (state, action) => {
    return postsAdapter.setAll(action.posts, state)
  }),
)

export function postsReducer (state: PostState | undefined, action: Action) {
  return _postsReducer(state, action)
}