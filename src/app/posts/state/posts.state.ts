import { createEntityAdapter, EntityState } from "@ngrx/entity"
import { Post } from "../../models/posts.model"

export interface PostState extends EntityState<Post> {
  count: number
}

export function sortByName(prev: Post, next: Post): number {
  return next.title.localeCompare(prev.title);
}

export const postsAdapter = createEntityAdapter<Post>({
  // selectId: (post: Post) => <string>post.id
  sortComparer: sortByName
})

export const initialState: PostState = postsAdapter.getInitialState({
  count: 0
})