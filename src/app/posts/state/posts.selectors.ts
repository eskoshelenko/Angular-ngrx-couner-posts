import { EntityState, Dictionary } from "@ngrx/entity";
import { createFeatureSelector, createSelector, DefaultProjectorFn, MemoizedSelector } from "@ngrx/store";
import { Post } from "src/app/models/posts.model";
import { RouterStateUrl } from "src/app/store/router/custom-serializer";
import { getCurrentRoute } from "src/app/store/router/router.selector";
import { postsAdapter, PostState } from "./posts.state";


export const POSTS_STATE_NAME = 'posts'

const getPostsState = createFeatureSelector<PostState>(POSTS_STATE_NAME)

export const postsSelectors = postsAdapter.getSelectors()

export const getPosts = createSelector(getPostsState, postsSelectors.selectAll)

export const getCount = createSelector(getPostsState, state => state.count)

export const getPostEntities = createSelector(
  getPostsState,
  postsSelectors.selectEntities
)

export const getPostById = createSelector(
  getPostEntities,
  getCurrentRoute,
  (posts, route: RouterStateUrl) => {
    return posts ? posts[route.params['id']] : undefined;
  }
)